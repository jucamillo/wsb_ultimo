<?php /* Template Name: Home */ ?>
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wsb
 */

get_header();
?>

		<?php
		while ( have_posts() ) :
			the_post(); ?>

	 	<div class="anima-back" id="anima">
	 		<div class="sq s1" id="l1">
		 		<span class="line1"></span>
		 	</div>
	 		<div class="sq s2" id="l2">
				<span class="line2"></span>
		 	</div>
	 		<div class="sq s3" id="l3">
				<span class="line3"></span>
		 	</div>
	 		<div class="sq s4" id="l4">
				<span class="line4"></span>
		 	</div>


		 	<div class="lbottom" >
		 		<span></span>
		 		<span></span>
		 		<span></span>
		 	</div>
		 	<div class="ltop">
		 		<span></span>
		 		<span></span>
		 		<span></span>
		 	</div>


		 	<div class="hortop">
		 		<span></span>
		 		<span></span>
		 	</div>


		 	<div class="tr_left" id="t1">
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/line2.svg" class="img">
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/line2.svg" class="img">
		 	</div>
		 	
		 	<!-- SIM	-->

		
		 	<!--

		 	<div class="tr_left t2" id="t2">
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/line2.svg" class="img">
		 	</div> -->



		 	<div class="tr_right" id="t3">
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/line1.svg" class="img">
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/line1.svg" class="img">
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/line1.svg" class="img">
		 	</div>
		
		 	<!-- SIM -->

		 	<!--
		 	<div class="tr_right t4" id="t4">
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/line1.svg" class="img">
		 	</div> -->



		 	<div class="tr_right t5" id="t5">
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/line1.svg" class="img">
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/line1.svg" class="img">
		 	</div>
		 
		 	<!-- SIM-->

		 	<!--
		 	<div class="tr_right t6" id="t6">
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/line1.svg" class="img">
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/line1.svg" class="img">
		 	</div>
			-->



		 	
		 	<div class="tr_left rt" id="t7">
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/line2.svg" class="img">
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/line2.svg" class="img">
		 	</div>
		<!-- SIM -->


		 	<!--
		 	<div class="tr_left rt" id="t8">
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/line2.svg" class="img">
		 	</div>
			-->

		 	<div class="map">
		 		
		 		<img src="<?php echo get_template_directory_uri(); ?>/images/map.svg" class="img">
		 	</div>

	 	</div>


				<section class="vc_section" id="home">
					<div class="container" >
							
						<div class="col-md-6 col-sm-12 col-xs-12" >
							<?php if( get_field('titulo') ): ?>
								<h1>
									<?php the_field('titulo'); ?>
								</h1>
							<?php endif; ?>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 rellax" data-rellax-speed="-2">
				    		<?php if ( have_rows('botao') ): ?>
				                <?php while ( have_rows('botao') ) : 
				                	the_row(); ?>
				                	<div class="wrapbtn"  id="">


		                			<?php $estilo_botao = get_sub_field('estilo_botao');

		                			//VIDEO BOTAO
									if( $estilo_botao && in_array('video', $estilo_botao) ) : ?>

			                        <a href="#vid1" class="btn_large video">
			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>
										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/images/play.svg">
										<?php endif; ?>
			                        </a>

									<?php 
		                			//DOWNLOAD BOTAO
									elseif ( $estilo_botao && in_array('download', $estilo_botao)) : ?>

										<?php if( get_sub_field('arquivo') ): ?>
										<a href="<?php the_sub_field('arquivo'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
										<?php else: ?>
										<a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
										<?php endif; ?>

			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>

										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/images/ico-bt-down.svg">
										<?php endif; ?>
			                        </a>

									<?php 
		                			//Botão padrão
		                			else: ?>
			                        <a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>
										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php endif; ?>
			                        </a>
				                    <?php endif; ?>
	
				                	</div>
				                <?php endwhile; ?>
				    		<?php endif; ?>	
						</div>
					</div>
				</section>

				<section class="vc_section" id="conteudo">
					<div class="container">
						<div class="col-md-6 col-sm-12 col-xs-12 rellax" data-rellax-speed="10">

							<div class="full" id="trigger2"></div>
							<div class="full" id="pin2">
							<?php if( get_field('titulo_2') ): ?>
								<h1>
									<?php the_field('titulo_2'); ?>
								</h1>
							<?php endif; ?>
							<?php if( get_field('texto_2') ): ?>
								<div class="txt">
									<?php the_field('texto_2'); ?>
								</div>
							<?php endif; ?>
							</div>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
						</div>
						<div class="col-lg-5 col-sm-12 col-md-5  col-xs-12 rellax" data-rellax-speed="-2">
							<?php
							// Check rows exists.
							if( have_rows('lista_2') ):
								echo '<ul class="blog-list">';
							    // Loop through rows.
							    while( have_rows('lista_2') ) : the_row(); ?>

								<?php if( get_sub_field('item') ): ?>
									<li>
										<h2>
											<?php the_sub_field('item'); ?>
										</h2>
									</li>
								<?php endif;
							    
							    endwhile;
								
								echo '</ul>';
							    // Do something...
							endif; ?>




				    		<?php if ( have_rows('botao_2') ): ?>
				                <?php while ( have_rows('botao_2') ) : the_row(); ?>
				                	<?php $estilo_botao = get_sub_field('estilo_botao');

		                			//VIDEO BOTAO
									if( $estilo_botao && in_array('video', $estilo_botao) ) : ?>

			                        <a href="#vid1" class="btn_large video">
			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>
										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/images/play.svg">
										<?php endif; ?>
			                        </a>

									<?php 
		                			//DOWNLOAD BOTAO
									elseif ( $estilo_botao && in_array('download', $estilo_botao)) : ?>

										<?php if( get_sub_field('arquivo') ): ?>
										<a href="<?php the_sub_field('arquivo'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
										<?php else: ?>
										<a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
										<?php endif; ?>

			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>

										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/images/ico-bt-down.svg">
										<?php endif; ?>
			                        </a>

									<?php 
		                			//Botão padrão
		                			else: ?>
			                        <a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>
										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php endif; ?>
			                        </a>
				                    <?php endif; ?>
				                <?php endwhile; ?>
				    		<?php endif; ?>	
				    		<div id="trigger3"></div>
						</div>
					</div>
				</section>


					
				<section class="vc_section" id="conteudo3">
					<div class="container">
						<div class="col-lg-6 col-md-7 col-sm-12  col-xs-12 rellax" data-rellax-speed="5">
							<?php if( get_field('titulo_3') ): ?>
									<h1>
										<?php the_field('titulo_3'); ?>
									</h1>
							<?php endif; ?>
				    		<?php if ( have_rows('botao_3') ): ?>

				                <?php while ( have_rows('botao_3') ) : the_row(); ?>
			                        <?php $estilo_botao = get_sub_field('estilo_botao');

		                			//VIDEO BOTAO
									if( $estilo_botao && in_array('video', $estilo_botao) ) : ?>

			                        <a href="#vid1" class="btn_large video">
			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>
										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/images/play.svg">
										<?php endif; ?>
			                        </a>

									<?php 
		                			//DOWNLOAD BOTAO
									elseif ( $estilo_botao && in_array('download', $estilo_botao)) : ?>

										<?php if( get_sub_field('arquivo') ): ?>
										<a href="<?php the_sub_field('arquivo'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
										<?php else: ?>
										<a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
										<?php endif; ?>

			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>

										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/images/ico-bt-down.svg">
										<?php endif; ?>
			                        </a>

									<?php 
		                			//Botão padrão
		                			else: ?>
			                        <a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>
										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php endif; ?>
			                        </a>
				                    <?php endif; ?>
				                <?php endwhile; ?>
				    		<?php endif; ?>	
						</div>
						<div class="col-lg-2 col-md-1 col-sm-6  col-xs-12">
				                <div class="space full"></div>
								<div class="full" id="pin4"></div>
						</div>
						<div class="col-lg-4  col-md-4 col-sm-6  col-xs-12 just-end  rellax" data-rellax-speed="-5">
							<?php if( get_field('subtitulo_3') ): ?>
								<h2>
									<?php the_field('subtitulo_3'); ?>
								</h2>
							<?php endif; ?>
						</div>
					</div>
				</section>


				<section class="vc_section" id="conteudo4">
					<div class="container">
						<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
							<div class="full" id="pin5">
								
								<?php if( get_field('titulo_4') ): ?>
									<h1>
										<?php the_field('titulo_4'); ?>
									</h1>
								<?php endif; ?>
					    		<?php if ( have_rows('botao_4') ): ?>
					                <?php while ( have_rows('botao_4') ) : the_row(); ?>
				                    
				                    <?php $estilo_botao = get_sub_field('estilo_botao');

		                			//VIDEO BOTAO
									if( $estilo_botao && in_array('video', $estilo_botao) ) : ?>

			                        <a href="#vid1" class="btn_large video">
			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>
										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/images/play.svg">
										<?php endif; ?>
			                        </a>

									<?php 
		                			//DOWNLOAD BOTAO
									elseif ( $estilo_botao && in_array('download', $estilo_botao)) : ?>

										<?php if( get_sub_field('arquivo') ): ?>
										<a href="<?php the_sub_field('arquivo'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
										<?php else: ?>
										<a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
										<?php endif; ?>

			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>

										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/images/ico-bt-down.svg">
										<?php endif; ?>
			                        </a>

									<?php 
		                			//Botão padrão
		                			else: ?>
			                        <a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>
										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php endif; ?>
			                        </a>
				                    <?php endif; ?>
					                <?php endwhile; ?>
					    		<?php endif; ?>	

				    			<img src="<?php echo get_template_directory_uri(); ?>/images/fileimg.svg" class="file">

				    			<!--

				    			<img src="<?php //echo get_template_directory_uri(); ?>/images/fileimg1.svg" class="file f2 rellax" data-rellax-speed="15"> -->
							</div>



						</div>
						<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">

	 						<?php echo do_shortcode( '[destaque_conteudo]' ); ?>
	 						<div class="w1">
	 							
								<div class="space3 full"></div>
								<div id="trigger5" class="full"></div>
	 						</div>


						</div>
					</div>
				</section>
		<?php
		endwhile; // End of the loop.
		?>

<script type="text/javascript">
var controller = new ScrollMagic.Controller();








// build scene
var scene = new ScrollMagic.Scene({
 duration: "100%",
 offset:0
})
.setTween("#l1, #l2", {width: "100%"}) 
.addTo(controller);


var scene2 = new ScrollMagic.Scene({
 duration: "100%",
 offset:0
})
.setTween("#l3, #l4", {height: "100%"}) 
.addTo(controller);

var scene4 = new ScrollMagic.Scene({
 duration: "100%",
 offset:0
})
.setTween(".hortop", {right: "0"}) 
.addTo(controller);


var scene3 = new ScrollMagic.Scene({
 duration: "100%",
 offset:0
})
.setTween(".lbottom, .ltop", {height: "100%"}) 
.addTo(controller);



var scene4 = new ScrollMagic.Scene({
 duration: "100%",
 offset:0
})
.setTween("#t1, #t5", {top: "0", left:"0"}) // #t2, #t6
.addTo(controller);



var scene5 = new ScrollMagic.Scene({
 duration: "100%",
 offset:0
})
.setTween("#t3,  #t7", {top: "0", right:"0"}) //#t4, #t8
.addTo(controller);





var scene6 = new ScrollMagic.Scene({
 duration: "90%",
	triggerElement: "#trigger3"
})
.setTween(".map", {opacity: "1"}) 
.addTo(controller);



var scene7 = new ScrollMagic.Scene({
 duration: "90%",
	triggerElement: "#trigger3"
})
.setTween("#l1, #l2, #l3, #l4, .hortop, .lbottom, .ltop, #t1, #t2, #t5, #t6, #t3, #t4, #t7, #t8", {opacity: "0"}) 
.addTo(controller);



var scene8 = new ScrollMagic.Scene({
	triggerElement: "#pin4"
})
.setClassToggle(".anima-back", "leave")
.addTo(controller);



var scenePin10 = new ScrollMagic.Scene({
	duration:(jQuery("ul.conteudos").height() - jQuery("#pin5").height()*1.5),
	triggerElement:"#trigger5"
 })
.setPin("#pin5")
.addTo(controller);



var rellax = new Rellax('.rellax', {
        center: true
      });

</script>
<?php
get_footer();
