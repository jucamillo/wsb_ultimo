<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package wsb
 */

get_header();
?>



<section class=" conteudo-sec mid single-content">
	<div class="container">
		<div class="col-lg-8 col-md-8">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php wsb_post_thumbnail(); ?>
				<h1>
					<?php the_title(); ?>
				</h1>
				<div class="meta">
	                <ul class="blog-categories">

	                	<?php $categories = wp_get_post_categories( get_the_ID() );
	                    //loop through them
	                    foreach($categories as $c){
	                      $cat = get_category( $c );
	                      //get the name of the category
	                      $cat_id = get_cat_ID( $cat->name );
	                      //make a list item containing a link to the category
	                       echo '<li><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'">'.$cat->name.'</a></li>';
	                    }
	                    ?>
	                </ul> 

	                <span><?php echo get_the_date(); ?></span>

				</div>

				<div class="content">
					<?php the_content(); ?>
				</div>

	    		<div class="share">
					<?php if( get_field('chamada', 'option') ): ?>
						<h3>
							<span><?php the_field('chamada', 'option'); ?></span>
						</h3>
					<?php endif; ?>
						<?php if(is_active_sidebar('share')){ dynamic_sidebar('share'); } ?>
	    		</div>

			<?php endwhile; ?>


		</div>
		<div class="col-lg-4 col-md-4">
			<?php echo get_search_form();?>

				<aside id="secondary" class="widget-area">
					<?php
					if(is_active_sidebar('singleblog')){
					dynamic_sidebar('singleblog');
					}
					?>
				</aside>
		</div>

	</div>
	
</section>

<script type="text/javascript">
	
        if (jQuery(window).width() < 992) {


            jQuery('.conteudo-sec.mid.single-content form.search-form').insertAfter('.post-thumbnail');
            /*
            $('.sec2 .owl-mob1').addClass('owl-carousel');

            setTimeout(
              function() 
              {
                $('.owl-mob1').owlCarousel({
                    loop:true,
                    margin:0,
                    responsiveClass:true,
                    dots: true,
                    nav:false,
                    autoHeight:true,
                    autoplay: false,
                    autoplayTimeout: 10000,
                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:1
                        },
                        768:{
                            items:1
                        },
                        1200:{
                            items:1
                        }
                    }
                })
              }, 1000); */
            
        }  
        

</script>
<?php
get_footer();
