<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wsb
 */

get_header();
?>


<section class="title-top conteudo-sec ttl-search-form">
	<div class="container">
		<div class="col-lg-8 col-md-7 col-xs-12">
			<?php if( get_field('titulo_cont', 'option') ): ?>
				<h1>
					<?php the_field('titulo_cont', 'option'); ?>
				</h1>
			<?php endif; ?>
		</div>
		<div class="col-lg-4 col-md-4 col-xs-12">
			<?php echo get_search_form();?>
		</div>
	</div>
</section>

<section class=" conteudo-sec mid">
	<div class="container">
		<div class="col-lg-8  col-md-8">
			<h2>
				<?php the_archive_title(); ?>
			</h2>

			<?php 
			if ( have_posts() ) :

            	echo '<ul class="conteudos">';

				while ( have_posts() ) :
					the_post();
	                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	                echo '<li>
			                <div class="sqimg">
	                        <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background-image: url('.$image[0].');">
	                        </a>
	                        </div>
	                        <div class="info">

		                        <ul class="blog-categories">';

		                        $categories = wp_get_post_categories( get_the_ID() );
		                            //loop through them
		                            foreach($categories as $c){
		                              $cat = get_category( $c );
		                              //get the name of the category
		                              $cat_id = get_cat_ID( $cat->name );
		                              //make a list item containing a link to the category
		                               echo '<li><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'">'.$cat->name.'</a></li>';
		                            }
		                        echo '
		                        </ul> 
	                            <span>'.get_the_date().'</span>
	                            <h3><a href="'.get_the_permalink().'" title="'.get_the_title().'"> '.get_the_title().'</a></h3>
						        <p>
						            '.strip_tags( get_the_excerpt() ).'
						        </p>
						        <a href="'.get_the_permalink().'" class="btn" title="'.__( 'Keep reading', 'wsb' ).'">
						        	'.__( 'Keep reading', 'wsb' ).'
						        </a>
                        </div>                 
	                    </li>';

				endwhile;
				echo "</ul>";
				wpbeginner_numeric_posts_nav();

			else :
				get_template_part( 'template-parts/content', 'none' );
			endif; 
			?>

		</div>
		<div class="col-lg-4  col-md-4">
			<?php get_sidebar(); ?>
		</div>

	</div>
	
</section>




<?php
//get_sidebar();
get_footer();
