<?php /* Template Name: Contato */ ?>
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wsb
 */

get_header();
?>

		<?php
		while ( have_posts() ) :
			the_post(); ?>
				<section class="vc_section" id="contato">
					<div class="container" >

							
						<div class="col-lg-6 col-md-6">
							<?php if( get_field('titulo') ): ?>
								<h1>
									<?php the_field('titulo'); ?>
								</h1>
							<?php endif; ?>
						</div>
						<div class="col-lg-6 col-md-6">
							
						</div>

						<div class="col-lg-3 col-md-3">
							<?php if( get_field('ttl_contato') ): ?>
								<h3><?php the_field('ttl_contato'); ?></h3>
							<?php endif; ?>
				    		<?php if ( have_rows('contato', 'option') ): ?>
				    			<ul class="contato">
				                <?php while ( have_rows('contato', 'option') ) : the_row(); ?>
				                    <li>

										<?php if( get_sub_field('link', 'option') ): ?>
				                        	<a href="<?php the_sub_field('link'); ?>" target="_blank">
										<?php endif; ?>
											<img src="<?php the_sub_field('icone'); ?>">
				                        	<span><?php the_sub_field('texto'); ?></span>
										<?php if( get_sub_field('link', 'option') ): ?>
				                        	</a>
										<?php endif; ?>
				                    </li>
				                <?php endwhile; ?>
				    			</ul>
				    		<?php endif; ?>						
						</div>
						<div class="col-lg-4 col-md-3">
							<?php if( get_field('ttl_social', 'option') ): ?>
								<h3><?php the_field('ttl_social', 'option'); ?></h3>
							<?php endif; ?>
				    		<?php if ( have_rows('redes_sociais', 'option') ): ?>
				    			<ul class="redes_sociais">
				                <?php while ( have_rows('redes_sociais', 'option') ) : the_row(); 
				                	$pos = get_sub_field('pos');
									if( $pos && in_array('footer', $pos) ) { ?>
				                    <li>
				                        <a href="<?php the_sub_field('link'); ?>" target="_blank">
				                        	<img src="<?php the_sub_field('icone'); ?>">
				                        	<span><?php the_sub_field('nome'); ?></span>
				                        </a>
				                    </li>
				                <?php } endwhile; ?>
				    			</ul>
				    		<?php endif; ?>					
						</div>
						<div class="col-lg-5 col-md-5">
							<?php if( get_field('short') ): ?>
								<?php the_field('short'); ?>
							<?php endif; ?>
						</div>




					</div>
				</section>
		<?php
		endwhile; // End of the loop.
		?>
<script type="text/javascript">
	/*
jQuery(document).delegate('.vc_section .wpcf7 form div.file', 'click', function(event) {
    event.preventDefault();
    jQuery(this).find('input').trigger( "click" );
});*/

jQuery(document).delegate('.smallname', 'click', function(event) {
    event.preventDefault();
    jQuery(this).addClass('on');
    jQuery(this).parent().find('input').focus();
    jQuery(this).parent().find('textarea').focus();
});
jQuery(".file input").change(function(e){
    var fileName = e.target.files[0].name;
    jQuery('.vc_section .wpcf7 form div.file strong + span').html(fileName);
});

jQuery(function(){

jQuery( ".vc_section .wpcf7-form-control-wrap input" ).each(function( index ) {
	var nome = jQuery(this).attr('placeholder');
	jQuery( "<span class='smallname'>" + nome + "</span>" ).insertBefore(this);
});
jQuery( ".vc_section .wpcf7-form-control-wrap textarea" ).each(function( index ) {
	var nome = jQuery(this).attr('placeholder');
	jQuery( "<span class='smallname'>" + nome + "</span>" ).insertBefore(this);
});

jQuery( ".vc_section input" ).focusout(function() {
	if(jQuery(this).val()==""){
    	jQuery(this).parent().find('.smallname').removeClass('on');
	}
  })


jQuery( ".vc_section textarea" ).focusout(function() {
	if(jQuery(this).val()==""){
    	jQuery(this).parent().find('.smallname').removeClass('on');
	}
  });

});

jQuery('.wpcf7 form span.wpcf7-list-item.first input').prop( "checked", true );


jQuery( "span.wpcf7-form-control-wrap.assunto" ).each(function( index ) {
	var assunto = jQuery('span.wpcf7-form-control-wrap.assunto input').val();
	jQuery( "<span class='selectfield'>" + assunto + "</span>" ).insertBefore('span.wpcf7-form-control-wrap.assunto > *');
});


jQuery( "span.wpcf7-form-control-wrap.posicao" ).each(function( index ) {
	var posicao = jQuery('span.wpcf7-form-control-wrap.posicao input').val();
	jQuery( "<span class='selectfield'>" + posicao + "</span>" ).insertBefore('span.wpcf7-form-control-wrap.posicao > *');
});




jQuery(document).delegate('span.wpcf7-form-control-wrap.assunto .selectfield', 'click', function(event) {
    event.preventDefault();
    jQuery('span.wpcf7-form-control-wrap.assunto').toggleClass('active');
});


jQuery(document).delegate('span.wpcf7-form-control-wrap.posicao .selectfield', 'click', function(event) {
    event.preventDefault();
    jQuery('span.wpcf7-form-control-wrap.posicao').toggleClass('active');
});

jQuery( '.vc_section input[name="posicao"]' ).change(function() {
	var nom = jQuery(this).val();
	jQuery('span.wpcf7-form-control-wrap.posicao .selectfield').html(nom);

    jQuery('span.wpcf7-form-control-wrap.posicao').removeClass('active');

});

jQuery( '.vc_section input[name="assunto"]' ).change(function() {
	console.log(jQuery(this).val());
	var nom = jQuery(this).val();
	jQuery('span.wpcf7-form-control-wrap.assunto .selectfield').html(nom);

    jQuery('span.wpcf7-form-control-wrap.assunto').removeClass('active');

	if(jQuery(this).val() == "Assunto" ){
		jQuery('#op1').removeClass('active');
		jQuery('#op2').removeClass('active');
		jQuery('#op3').removeClass('active');
		jQuery('.geral').removeClass('active');
	} 
	if(jQuery(this).val() == "Quero entrar em contato"){
		jQuery('#op1').removeClass('active');
		jQuery('#op2').removeClass('active');
		jQuery('#op3').removeClass('active');
		jQuery('.geral').addClass('active');
	}
	if(jQuery(this).val() == "Quero fazer um acordo"){
		jQuery('#op1').addClass('active');
		jQuery('#op2').removeClass('active');
		jQuery('#op3').removeClass('active');
		jQuery('.geral').addClass('active');
	} 
	if(jQuery(this).val() == "Quero trabalhar com vocês"){
		jQuery('#op2').addClass('active');
		jQuery('#op1').removeClass('active');
		jQuery('#op3').removeClass('active');
		jQuery('.geral').addClass('active');
	} 
	if(jQuery(this).val() == "Quero ser um advogado correspondente"){
		jQuery('#op3').addClass('active');
		jQuery('#op2').removeClass('active');
		jQuery('#op1').removeClass('active');
		jQuery('.geral').addClass('active');
	}
});
</script>
<?php
get_footer();
